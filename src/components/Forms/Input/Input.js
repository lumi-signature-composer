import React from 'react';
import './Input.css';

const Input = function({ label, type, name, autoFocus, onChange }) {
  let parsedName = name || label.toLowerCase();

  return (
    <div className="Input-wrapper">
      <label className="Input-label">
        {label}
      </label>
      <input
        type={type}
        name={parsedName}
        onChange={onChange}
        autoFocus={autoFocus}
        className="Input-input"
      />
    </div>
  );
};

export default Input;
